<p align="center">
    <h1 align="center">hiit</h1>
    <h3 align="center"><em>workout tool</em></h3>
</p>
<p align="center">
  <img src="screenshot.png" alt="quotify">
</p>

---
<span style="font-size:1.2em;">
This project provides flexible countdown timers with audio cues for <a href="https://en.wikipedia.org/wiki/High-intensity_interval_training">high intensity interval training </a>
</span>

---

## about HIIT

The HIIT method is a workout/training method supposed to maximise the benefits of short training periods. The validity of the method and it's benefits and shortcommings being way beyond the scope of this tool, please **do your own research** if you are interested in this subject.

The basic principle behind the hiit method is to alternate short period of intense exercise with recovery periods until physical exhaustion. It needs precise monitoring of the time spent doing the exercise and recovering as well, which may not be easily achieved when working out alone.

## using countdown timers

In order to workout in a controlled fashion, this project serves a web pages containing a countdown timer alternating between *work* and *rest* phases, each one playing it's own customisable sound in the browser. One glance at the screen reveal how much of the phase is left and how much of the exercise remains.

Once the exercise is defined, it can be done while listening to a podcast, the audio cues indicating when to rest and when to work. Multiples exercises can be stored and recalled and the exercise can be paused, should you have to.

## adding an exercice

![](edit.png)

Let's say you're trying to get better at planking. Start a stopwatch and get to it. When you cannot take it anymore, note the time achieved and use it as a basis to create a new exercise. Fill the **duration** field with this value, minus ~10% The **repetition** field can be set to a large number until we have assessed the maximum repetitions before exhaustion. Then set a name, a short description if needed and the exercise is ready to use.

The **get ready** field adds a *get set* phase between the *recovery period* and the *next working phase* to allow for a preparation (getting up or down, grabbing an accessory...) This time is deducted from the recovery period, set it to zero if not needed.

## exercising

Now the fun begins : load the exercise by clicking on it's name on the top and start it by clicking on the big stopwatch in the middle. When each phase alternates, the stopwatch changes color and a sound is played. The progress bar on the top always indicates the global progression of the exercise. Another click on the stopwatch pause the session, should the phone start ringing.

When the exercise is over, a melody will helpfully compensate for the efforts and the next exercise can be loaded by using the navigation arrows on the side.

## about this project

The interface is built with [React](https://en.reactjs.org/) with [bootstrap](https://getbootstrap.com/) while the API is written in python using [FastAPI](https://fastapi.tiangolo.com/) and served by [uvicorn](https://www.uvicorn.org/). The database is stored using [mySQL](https://www.mysql.com/fr/) and accessed in the API via [mySQL.connector](https://dev.mysql.com/doc/connector-python/en/). The project is fully containerized and will run out of the box with docker-compose

## usage
Running the project from a fresh clone within a machine with docker and docker-compose installed can be done in minutes by a single `docker-compose up -d` (or `podman-compose up -d` on centos/redhat/fedora). All environment variables have a default value provided in the *docker-compose.yaml* file, no additionnal setup is needed. A development version will be served from the [localhost:8080](http://localhost:8080)
To futher customise the configuration, a *.env* file at the project root can be created to overwrite the default variables. A sample *.env.example* containing every customisable variable is provided with suitable documentation.

### dev mode
By setting the `ENV=dev` variable, the *dev* target will be selected for both frontend and backend builds. This will bind-mount the sources inside the containers to allow the hot module reloading to work in both applications. They will run in development mode with extended debug information.

### prod mode
`ENV=prod` will select *prod* target builds for both the frontend and backend. The frontend sources will be compiled into a static build then served via *NodeJS* in production-mode. The *npm* packages will be installed in *ci* mode from the *package-lock.json* exclusively. The python backend will run from the sources with hmr disabled and production-ready optimisations.