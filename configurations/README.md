# configurations
Some variables used by both the API and the frontend may be defined via environnement variables. For ease of use, they are regrouped inside theses files with the `set -a` switch to automatically export them. They are then `source`d by other scripts, including **run**, **deploy** and **runTests**. At least two configuration should be defined : *debug* and prod. 

Secret values like SSH and SQL passwords may be stored in *.secret* files which are gitignored (*debug.secret*, *prod.secret*...).

- **API** variables :
    - `API_HOST`
    - `API_PORT`
    - `SECRET_KEY` : salt for the hashs, obtained by `openssl rand -hex 32`


- **database** variables :
    - `DB_HOST`
    - `DB_USER`
    - `DB_NAME`
    - `DB_PASSWORD`
- **frontend** variables :
    - `HTTP_HOST`
    - `HTTP_PORT`
    - `REACT_APP_API_ENDPOINT`: url used by the frontend to reach the API, defaults to http://**$API_HOST**:**$API_PORT**/api
 
- **deployment** variables :
    - `DEPLOY_FOLDER` local folder where the build will be stored
    - `SSH_USER`
    - `SSH_PORT` defaults to 22
    - `REMOTE_FRONTEND_PATH` path used to store the content of the *DEPLOY_FOLDER/frontend* folder to the remote server
    - `REMOTE_BACKEND_PATH` path used to store the content of the *DEPLOY_FOLDER/backend* folder to the remote server
