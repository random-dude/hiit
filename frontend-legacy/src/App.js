import React, { useState, useEffect } from "react";
import CurrentExercise from "./components/currentExercise/currentExercise";
import PlayAudio from "./components/PlayAudio";
import Navbar from "./components/Navbar";
import NavButtons from "./components/currentExercise/NavButtons";
import NewExerciseModal from "./components/NewExerciseModal";
import LoginModal from "./components/loginModal";
import UserParametersModal from "./components/userParametersModal";
import LoadingScreen from "./components/LoadingScreen";
import { getCookie, setCookie, removeCookie } from "./components/cookies";
import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {

  const apiURL = process.env.REACT_APP_API_ENDPOINT

  // --------------------- react hooks ---------------------

  const [database, setDatabase] = useState([]);
  const [exerciseIndex, setExerciseIndex] = useState(0); // navbar callback to change selected exercise
  const [config, setConfig] = useState(undefined)
  const [username, setUsername] = useState("anonymous");
  const [token, setToken] = useState(undefined);

  // restore username and token from cookies
  useEffect(() => {
    const [userName, jwtToken] = [getCookie("username", "anonymous"), getCookie("token")];
    // user has a login cookie
    if (userName !== "anonymous" && jwtToken !== undefined){
      // this will trigger an effect hook to fetch db and config
      setUsername(userName);
      setToken(jwtToken);
    }
  }, [])

  // set cookies when username or token is updated
  // cookies are set from the frontend to avoid CORS restrictions
  useEffect(()=>{
      fetchDatabase();
      fetchConfig();
      setCookie("token", token);
  }, [token]);

  // set username cookie when username is modified
  useEffect(()=>{if (username !== "anonymous") setCookie("username", username);}, [username]);

  // set database cookie on db modification for anonymous users
  useEffect(()=>{
    if (username === "anonymous" && database !== undefined) setCookie("database", database)
  }, [database]);

  // set configuration cookie on db modification for anonymous users
  useEffect(()=>{
    if (username === "anonymous" && config !== undefined) setCookie("configuration", config);
  }, [config]);

  // ---------------------- functions ----------------------

  // FIXME debug
  const clearCookies = () => {
    ["database", "token", "username", "configuration"].forEach(v=>removeCookie(v));
  }
  document.clearCookies = clearCookies;

// returns headers dict with or without the JWT Authorization token
const getJsonHeaders = () => {
  return (token !== undefined) ? {"Content-Type": "application/json", Authorization: "Bearer " + token} : {"Content-Type": "application/json"};
}

// CurrentExercise cb to play sound
const audioPlayer=React.createRef()
const playSoundFor = (status)=>{
  audioPlayer.current.play(status)
}

// FIXME use config context
// navbar callback to toggle sound ON/OFF
const toggleSound = ()=>{
  updateConfig({isSoundOn:!config.isSoundOn})
};

// will retrieve either the anonymous default config or the user config if token is set
const fetchConfig = () =>{
  // if the user is anonymous, retrieve db from cookie or fetch anonymously
  if (username === "anonymous"){
    const previousConfig = getCookie("configuration", undefined, true);
    if (previousConfig !== undefined){
      setConfig(previousConfig);
      return;
    }
  }
  // if user is logged in or no cookies were found
  fetch(apiURL + "/config", {
    method:"GET",
    headers: getJsonHeaders()
  }).then (r=>r.json()).then(setConfig).catch(e=>setTimeout(fetchConfig, 500))
}

// get a {property:value} object and update the corresponding
// field in db then get the updated config from the response
const updateConfig = (item) => {
  if (username === "anonymous") setConfig({...config, ...item});
  else {
    fetch(apiURL + "/config", {
      method: "POST",
      headers: getJsonHeaders(),
      body: JSON.stringify(item)
    })
    .then(response=>response.json().then(setConfig)); 
  }
}

// returns the exercise list an retry automatically if API doesn't answer
const fetchDatabase = () =>{
  // if the user is anonymous, retrieve db from cookie or fetch anonymously
  if (username === "anonymous"){
    const previousDatabase = getCookie("database", undefined, true);
    if (previousDatabase !== undefined){
      setDatabase(previousDatabase);
      return;
    }
  }
  // if user is logged in or no cookies were found
  fetch(apiURL + "/getExercises", {
    method: "GET",
    headers: getJsonHeaders()
  }).then(r=>r.json())
  .then(setDatabase)
  .catch(e=>setTimeout(fetchDatabase, 500));
}


  // will modify the database by adding or modifying an exercise
  // via a POST request encoding the exercise object in the body
  // It will receive a response containing all the exercises up to date
  const saveExercise = async  (exercise) => {
    const action = (database.some(e => e.id === exercise.id)) ? "update" : "add";
    if (username !== "anonymous"){
      fetch(apiURL + "/" +action, {
      method: "POST",
      headers: { "Content-Type": "application/json", Authorization: "Bearer " + token },
      body: JSON.stringify(exercise)
    })
    .then(response=>response.json().then(setDatabase)); 
  }
  // anonymous user : only modify the local variable database which is stored in the cookies
  else if (action == "add") setDatabase([...database, exercise]);
  else setDatabase(database.map(e =>(e.id === exercise.id)? exercise : e))
};

  const deleteExercise = async (index) => {
    if (database.length === 1) return // the last exercise is here to stay...
    const id = database[index].id;
    // select previous exercise knowing that database.length isn't updated yet
    const prevIndex = exerciseIndex > 1 ? exerciseIndex - 1 : database.length - 2;
    setExerciseIndex(prevIndex);
    // user is logged in, we'll need to alter it's database on the API
    if (username !== "anonymous") {
      fetch(apiURL + `/remove${id}`, {method: 'DELETE', headers: {Authorization: "Bearer " + token}})
      .then(response=>response.json().then(setDatabase));
    }
    // anonymous user : no need to fetch, we'll remove it from the local variable which will be written as a cookie
    else setDatabase(database.filter((e, i) => i !== index))
  }

  const login = (username, password, addExercises=null) =>{
    return new Promise((resolve, rejects) => {
    fetch(apiURL + "/login", {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ username: username, password: password }),
    }).then(response => {
      // 401 Unauthorized
      if (response.status === 401) resolve(false);
      else if (response.status === 200) {
        // globally set the token
        response.json().then(setToken);
        setUsername(username);
        // if this is the first login after signing up, add the pre-existing exercises
        // stored into the database cookie
        if (addExercises !== null) {
          addExercises.forEach(e=>saveExercise(e))
          // update the UI after the exercises were saved
          setTimeout(fetchDatabase, 200);
        }
        removeCookie("database");
        resolve (true);
      }
    });
  });
  }

// FIXME use logging context
  const logout = () => {
    clearCookies();
    setUsername("anonymous");
  }

  return (
    // waiting for the async fetch to resolves
    (database.length === 0 || config === undefined) ? <LoadingScreen/> : (

    <React.Fragment>

      <Navbar 
      isSoundOn = {config.isSoundOn}
      isLoggedIn = {(token !== undefined)}
      toggleSound={toggleSound}
      database={database}
      selected={exerciseIndex}
      selectExercise={setExerciseIndex}
      deleteExercise={deleteExercise}
      >
      </Navbar>
      
      <CurrentExercise 
      config={config}
      exercise={database[exerciseIndex]}
      audioplayer={playSoundFor}
      key={Math.random()}
      >
      </CurrentExercise>

      <NavButtons 
      setExerciseIndex={setExerciseIndex}
      exerciseIndex={exerciseIndex}
      dbLength={database.length}>
      </NavButtons>
    
      <NewExerciseModal
      exerciseID={database[database.length - 1].id + 1}
      modalID="addExerciseModal"
      saveExercise={saveExercise}
      />

      <NewExerciseModal
      exerciseID={database[exerciseIndex].id}
      exercise = {database[exerciseIndex]}
      modalID="editExerciseModal"
      saveExercise={saveExercise}
      key={Math.random()}
      />

      <LoginModal apiURL={apiURL} login={login}/>

      <UserParametersModal username={username} setUsername={setUsername} logout={logout} login={login} apiURL={apiURL} token={token} setToken={setToken}/>

      <PlayAudio config={config} ref={audioPlayer}/>

    </React.Fragment>
    )

  );
}

export default App;
