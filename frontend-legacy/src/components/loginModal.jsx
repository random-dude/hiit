import React, { useState, useRef, useEffect } from "react";
import { getCookie } from "./cookies";

export const LoginModal = props => {
  // ------------------------------- hooks -------------------------------

  // used to toggle between login mode and sign up mode
  const [isSigningUp, setIsSigningUp] = useState(false);
  // JSX for the status text at the bottom of this modal
  const [statusText, setStatusText] = useState("");
  // diy captcha populated by a text question and a numerical answer
  const [captcha, setCaptcha] = useState({ question: "", answer: 0 });

  // refs of the inputs fields
  const inputsData = {
    username: useRef(null),
    password: useRef(null),
    confirmation: useRef(null),
    captcha: useRef(null),
  };

  // clear the passwords fields and status text on mode change and regen captcha
  useEffect(() => {
    setStatusText("");
    Object.values(inputsData).forEach(i => {
      if (i.current !== null) i.current.value = "";
    });
    generateCaptcha();
  }, [isSigningUp]);

  // ----------------------------- functions -----------------------------

  // returns a random int between min and max
  const randomInt = (min, max) => Math.floor(Math.random() * (max - min + 1) + min);

  // transforms the login modal into a signup modal with captcha
  const setSignup = () => {
    generateCaptcha();
    setStatusText("");
    setIsSigningUp(true);
  };

  // generate a random question and store the expected answer into the captcha variable
  const generateCaptcha = () => {
    const [left, right] = [randomInt(1, 10), randomInt(1, 10)];
    const question = `if I hold ${right} dumbbells in my right hand, ${left} dumbbells in my left hand and \
    ${randomInt(1, 10)} on my head, how many do I have in hands ?`;
    setCaptcha({ question: question, correctAnswer: `${left + right}` });
  };

  // make sure that each input gets populated and validate the captcha before calling the API
  // this function will be called from both the login and the signup buttons
  const validateForm = async e => {
    let statusMessage = [];
    let isValidated = true;

    // verify that username, password and confirmation are populated
    ["username", "password", "confirmation"].forEach(key => {
      if (inputsData[key].current !== null && inputsData[key].current.value === "") {
        statusMessage.push(
          <p className="text-warning my-1" key={key}>
            please input the {key}
          </p>
        );
        isValidated = false; // TODO refactor by replacing isValidated by statusMessage.length === 0
      }
    });

    // validates the captcha
    if (isSigningUp) {
      if (inputsData.captcha.current.value === "") {
        statusMessage.push(<p className="text-warning my-1">please answer to the captcha question</p>);
        isValidated = false;
      } else if (inputsData.captcha.current.value !== captcha.correctAnswer) {
        statusMessage.push(
          <p className="text-danger my-1">sorry, seems like you failed at elementary maths, please try again</p>
        );
        generateCaptcha();
        isValidated = false;
      }
      // validates the password confirmation
      if (inputsData.password.current.value !== inputsData.confirmation.current.value) {
        statusMessage.push(<p className="text-warning">the confirmation does not match your password</p>);
        isValidated = false;
      }
    }

    // call the API or exit this function
    if (isValidated) {
      const username = inputsData.username.current.value;
      const password = inputsData.password.current.value;

      setStatusText(
        isSigningUp ? (
          <p>
            registering new user <b>{username}</b>...
          </p>
        ) : (
          <p>logging in ...</p>
        )
      );

      // sign up API call
      if (isSigningUp) {
        fetch(props.apiURL + "/signup", {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify({ username: username, password: password }),
        }).then(response => {
          if (response.status === 201) {
            setStatusText(
              <p>
                <b>successfully signed up and logged in !</b>
              </p>
            );
            const databaseCookie = getCookie("database", undefined, true);
            // auto login and add cookie stored exercises into db
            props.login(username, password, databaseCookie !== undefined ? databaseCookie : null).then(loggedIn => {
              if (loggedIn) document.querySelector("#loginModal").click();
            });
          } else if (response.status === 409)
            setStatusText(
              <p className="text-danger">
                the user <b>{username}</b> already exists, please choose another username
              </p>
            );
        });
      }

      // Log in API call via props.login
      else {
        props.login(username, password).then(loggedIn => {
          if (loggedIn) document.querySelector("#loginModal").click();
          else
            setStatusText(<p className="text-danger">error logging in, please check your username and/or password</p>);
        });
      }
    } else setStatusText(statusMessage);
  };

  const getInput = (label, ref, isPassword = false) => (
    <div className="row text-center">
      <label className="col-sm-3 col-form-label my-auto">{label}</label>
      <div className="col-sm-9">
        <input
          type={isPassword ? "password" : "text"}
          className="form-control mb-2"
          ref={ref}
          key={label}
          id={label}
          defaultValue=""
          required
        />
      </div>
    </div>
  );

  // ----------------------------- Markup -----------------------------

  return (
    <div className="modal fade unselectable" tabIndex="-1" id="loginModal">
      <div className="modal-dialog modal-dialog-centered mx-auto">
        <div className="modal-content">
          <div className="modal-header mx-auto">
            <h2>{isSigningUp ? "sign up" : "login / signup"}</h2>
          </div>
          <div className="modal-body">
            {/* login/sign up form */}
            <form>
              {/* username input */}
              {getInput("username", inputsData.username)}
              {/* password input */}
              {getInput("password", inputsData.password, true)}
              {/* password confirmation input */}
              {isSigningUp && getInput("confirm", inputsData.confirmation, true)}
              {/* captcha input */}
              {isSigningUp && (
                <div className="row">
                  {isSigningUp && <p className="mt-4 mb-1">{captcha.question}</p>}
                  <label className="col-sm-3 col-form-label my-auto">answer</label>
                  <div className="col-sm-9">
                    <input
                      type="text"
                      className="form-control mb-2"
                      id="captcha"
                      ref={inputsData.captcha}
                      defaultValue=""
                      required
                    />
                  </div>
                </div>
              )}
            </form>

            {/* sign up now section */}
            {!isSigningUp && (
              <>
                <h5 className="text-center pt-2">not registered ?</h5>
                <p className="mb-0">
                  Sign up to create your own exercises and access them from any device. It's fast and free, no email
                  asked.
                </p>
              </>
            )}
            <div className="text-center">
              {isSigningUp || (
                <button type="button" className="btn btn-primary my-3" onClick={setSignup}>
                  Signup
                </button>
              )}
            </div>
          </div>

          {/* status text and spinner */}
          {statusText && <div className="row text-center">{statusText}</div>}

          {/* login, signup and exit buttons */}
          <div className="my-3 mx-auto">
            <button type="button" className="btn btn-secondary mx-2" data-bs-dismiss="modal">
              Close
            </button>
            {(isSigningUp && (
              <React.Fragment>
                <button type="button" className="btn btn-secondary mx-2" onClick={() => setIsSigningUp(false)}>
                  Login
                </button>
                <button type="submit" className="btn btn-primary mx-2" onClick={validateForm}>
                  Signup
                </button>
              </React.Fragment>
            )) || (
              <button type="submit" className="btn btn-primary mx-2" onClick={validateForm}>
                Login
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default LoginModal;
