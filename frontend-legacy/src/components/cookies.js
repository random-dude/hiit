
  // set a cookie for a given variable, including arrays and objects
export const setCookie = (varname, value, expireDays=7, sameSite="Strict", path="/") => {
    let expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() + expireDays);
    const cookiesOptions = `expires=${expirationDate.toUTCString()}; path=${path};SameSite=${sameSite};`;
    value = (typeof value === "object" || typeof value === "boolean") ? JSON.stringify(value) : value;
    document.cookie = `${varname}=${value}; ${cookiesOptions}`;
  }

  // get the value of a given variable in the cookies, returns a default value if not set
export const getCookie = (varname, defaultValue=undefined, isJson=false) => {
    varname += "=";
    if (document.cookie.length > 0 && document.cookie.includes(varname)){
      const value = unescape(document.cookie.split(varname).pop().split(";")[0]);
      return (isJson)? JSON.parse(value) : value
    }
    else return defaultValue;
}

// remove a cookie by setting it's expiration date to yesterday
export const removeCookie = (varname) => {
  if (document.cookie.length > 0 && document.cookie.includes(varname)) {
    let expirationDate = new Date();
    expirationDate.setDate(expirationDate.getDate() - 1);
    document.cookie = `${varname}=0; expires=${expirationDate.toUTCString()};`;
  }
}