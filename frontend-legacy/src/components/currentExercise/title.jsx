import React, { Component } from "react";

export class Title extends Component {
  render() {
    return <h1 className="display-4 title">{this.props.text}</h1>;
  }
}

export default Title;
