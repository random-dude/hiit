import React, { Component } from "react";
import { FaCaretSquareLeft, FaCaretSquareRight } from "react-icons/fa";

export class NavButtons extends Component {
  iconSize = 30;
  iconColor = "white";

  prevExercise = () => {
    const index = this.props.exerciseIndex !== 0 ? this.props.exerciseIndex - 1 : this.props.dbLength - 1;
    this.props.setExerciseIndex(index);
  };

  nextExercise = () => {
    const index = this.props.exerciseIndex === this.props.dbLength - 1 ? 0 : this.props.exerciseIndex + 1;
    this.props.setExerciseIndex(index);
    console.log("next exercise", index);
  };

  render() {
    const overlayStyle = {
      position: "fixed",
      top: "50%" /* 25% from the top */,
      width: "100%" /* 100% width */,
      textAlign: "center" /* Centered text/links */,
      marginTop: "30px" /* 30px top margin to avoid conflict with the close button on smaller screens */,
      zIndex: "0",
    };

    return (
      <div className="d-flex justify-content-between" style={overlayStyle}>
        <span className="mx-3" onClick={this.prevExercise}>
          <FaCaretSquareLeft size={this.iconSize} color={this.iconColor} />
        </span>
        <span className="ms-auto mx-3" onClick={this.nextExercise}>
          <FaCaretSquareRight size={this.iconSize} color={this.iconColor} />
        </span>
      </div>
    );
  }
}

export default NavButtons;
