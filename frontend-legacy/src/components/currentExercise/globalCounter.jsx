import React, { Component } from "react";

export class GlobalCounter extends Component {
  // returns a padded string of the whole exercise duration formatted as MM:SS
  humanReadableDuration = () => {
    const totalSeconds = this.props.duration;
    const minutes = Math.floor(totalSeconds / 60);
    const seconds = Math.round(totalSeconds % 60);
    return minutes.toString().padStart(2, "0") + "m" + seconds.toString().padStart(2, "0") + "s";
  };

  render() {
    return (
      <div>
        <div className="progress mx-auto my-3" style={{ width: "60%" }}>
          <div
            className="progress-bar bg-info progress-bar-striped progress-bar-animated"
            style={{ width: Math.round(100 * this.props.progression).toString() + "%" }}></div>
        </div>
        <p className="pb-3 mx-auto">total duration {this.humanReadableDuration()}</p>
      </div>
    );
  }
}

export default GlobalCounter;
