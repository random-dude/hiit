import React, { Component } from "react";
import { CircularProgressbar, buildStyles } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

export class PhaseCounter extends Component {
  render() {
    const statusColor = ["#03d7fc", "#03d7fc", "#fc9d03", "#03fc90", "#03fc90"][this.props.status];
    const percentage = this.props.status === 0 ? "0" : 100 - this.props.progression;
    return (
      <div className="mx-auto unselectable" style={{ width: "40vh", height: "40vh" }}>
        <CircularProgressbar
          value={percentage}
          counterClockwise
          text={`${this.props.message}`}
          // background
          // backgroundPadding={4}
          // strokeWidth={6}
          // backgroundColor: statusColor,
          // pathColor: "#fff",
          strokeWidth={6}
          styles={buildStyles({
            // textColor: statusColor,
            pathColor: statusColor,
            textColor: "#fff",
            trailColor: "transparent",
            textSize: "1rem",
            // animation speed and ease varies when the bar is filled or emptied
            pathTransition: percentage === 100 ? "stroke-dashoffset 0.15s ease 0.03s" : "stroke-dashoffset .4s ease 0s",
          })}
        />
      </div>
    );
  }
}

export default PhaseCounter;
