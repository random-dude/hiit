import React, { Component } from "react";

export class PauseOverlay extends Component {
  overlayStyle = {
    position: "fixed",
    width: "100%",
    height: "100%",
    top: "0",
    left: "0",
    right: "0",
    bottom: "0",
    backgroundColor: "rgba(0,0,0,0.8)",
    zIndex: "2",
    cursor: "default",
  };

  overlayContentStyle = {
    position: "absolute",
    top: "50%",
    left: "50%",
    fontSize: "50px",
    color: "white",
    opacity: "0.8",
    width: "10%",
    transform: "translate(-50%,-50%)",
    // -ms-transform: "translate(-50%,-50%)",
  };

  render() {
    this.overlayStyle = {...this.overlayStyle, display:(this.props.isShown)?"block":"none"}
    return (
      <div id="pause-overlay" style={this.overlayStyle}>
        <img
          src={process.env.PUBLIC_URL + "/img/paused.png"}
          id="pause-overlay-content"
          style={this.overlayContentStyle}
          alt="paused"></img>
      </div>
    );
  }
}

export default PauseOverlay;
