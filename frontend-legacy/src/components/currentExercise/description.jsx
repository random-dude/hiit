import React, { Component } from "react";

export class Description extends Component {
  render() {
    return <p className="pb-3 mx-5">{this.props.text}</p>;
  }
}

export default Description;
