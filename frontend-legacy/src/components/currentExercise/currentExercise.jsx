import React, { Component } from "react";
import Title from "./title";
import Description from "./description";
import GlobalCounter from "./globalCounter";
import PhaseCounter from "./phaseCounter";
import PauseOverlay from "./PauseOverlay";

export class CurrentExercise extends Component {
  interval = 0; // will store the ID value of the timer returned by setInterval
  showOverlay = false;

  constructor(props) {
    super();
    // config contains system wide parameters that won't depend on the exercise (sound files, timer interval ...)
    this.config = props.config;
    // adding status related variables to the state
    this.state = {
      ...props.exercise,
      ...this.getRestAndTotalDuration(props.exercise),
      progression: 0,
      status: 0, // 0=notStarted, 1=getSet, 2=work, 3=rest, 4=finished 5=ready for more
      currentIteration: 0,
      secondsElapsed: 0,
      totalSecondsElapsed: 0,
      isPaused: true,
    };
    this.maxTimers = [0, this.state.getSet, this.state.duration, this.state.rest, 2, 0];
    this.baseState = { ...this.state }; // used to reset the component
  }

  // calculate rest time and total duration and return them as an object
  getRestAndTotalDuration = exercise => {
    const rest = Math.max(Math.round(exercise.duration / 2) - exercise.getSet, 0);
    const duration = exercise.repetitions * (exercise.duration + exercise.getSet + rest) - rest;
    return { rest: rest, totalDuration: duration };
  };

  // (re)start the exercise or unpause it
  play = () => {
    let state = { ...this.state };
    this.showOverlay = false;
    if (state.status >= 4) this.reset();
    else {
      console.log("starting exercise", state.title);
      state.isPaused = false;
      this.interval = setInterval(this.incrementTimer, this.config.timeIncrement);
      this.setState(state, () => {
        if (state.status === 0) this.nextStatus();
      });
    }
  };

  // clear the timer and show the pauseOverlay
  pause = () => {
    console.log("paused");
    this.setState({ ...this.state, isPaused: true });
    clearInterval(this.interval);
    this.showOverlay = true;
  };

  // reset by restoring the state to the post-constructor state
  reset = () => {
    console.log("resetting the exercise");
    this.setState({ ...this.baseState }, () => this.play());
    clearInterval(this.interval);
  };

  playPauseToggle = () => {
    if (this.state.isPaused) this.play();
    else this.pause();
  };

  //   updates the progress bar and switch to the next status if needed
  incrementTimer = () => {
    if (this.state.secondsElapsed < this.maxTimers[this.state.status]) {
      let state = { ...this.state };
      state.secondsElapsed += this.config.timeIncrement / 1000; // ms->s
      state.totalSecondsElapsed += this.config.timeIncrement / 1000;
      state.progression = state.totalSecondsElapsed / state.totalDuration;
      this.setState(state);
    } else {
      this.nextStatus();
    }
  };

  getHRstatus = id => {
    return ["click to start", "get ready", "work", "rest", "well done", "next one"][id];
  };

  // manage the logic of changing status and iterating over repetitions
  // status desctiptions : 0=click to start, 1=get ready, 2=work, 3=rest, 4=finished 5=ready for more
  nextStatus = () => {
    let state = { ...this.state };

    // last step of an iteration
    if (state.status === 3 && state.currentIteration < state.repetitions - 1) {
      state.currentIteration++;
      console.log("new iteration : ", state.currentIteration + 1, "/", state.repetitions);
      state.status = 0;
      state.secondsElapsed = 0;
    }

    // last step of the last iteration : we skip the resting period
    if (state.status >= 2 && state.currentIteration === state.repetitions - 1) {
      console.log("finished exercise, well done !");
      clearInterval(this.interval);
      state.status = 4;
      this.playAudio(4);
      state.isPaused = true;
    }

    // normal status
    if (state.status < 3) {
      if (state.status === 0 && state.getSet < 1) state.status++; // skip getset
      console.log("changing status from", this.getHRstatus(state.status), "to", this.getHRstatus(state.status + 1));
      state.status++;
      state.secondsElapsed = 0;
      if (this.maxTimers[state.status] > 0) {
        this.playAudio(state.status);
      }
    }
    this.setState(state);
  };

  playAudio = status => {
    // we must pass the name of the key in App.js-> config (same as the configuration table in db)
    if (!this.config.isSoundOn) return;
    const audioName = ["notStarted", "getSetAudio", "goAudio", "restAudio", "congratsAudio"][status];
    this.props.audioplayer(audioName);
  };

  // calculate the current step progression and return it as a rounded percentage string ("98")
  getStepProgression = () => {
    if (this.state.status > 0)
      return Math.round((100 * this.state.secondsElapsed) / this.maxTimers[this.state.status]).toString();
    return "";
  };

  componentWillUnmount() {
    if (!this.state.isPaused) clearInterval(this.timer);
  }

  render() {
    return (
      <div>
        {/* onClick MUST be placed on this outside div for it to work */}
        <div onClick={this.playPauseToggle} className="unselectable">
          <PauseOverlay isShown={this.showOverlay} />
        </div>

        <div className="jumbotron text-center unselectable pb-0 pt-1 my-0">
          <Title text={this.state.title} />
          <GlobalCounter
            duration={this.state.totalDuration}
            progression={this.state.progression}
            status={this.state.status}
          />
          <Description text={this.state.description} />
          <div onClick={this.playPauseToggle}>
            <PhaseCounter
              message={this.getHRstatus(this.state.status)}
              progression={this.getStepProgression()}
              status={this.state.status}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CurrentExercise;
