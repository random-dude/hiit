import React, { Component } from "react";
// FIXME validate titles, descriptions to fit database char limit

export class NewExerciseModal extends Component {
  constructor(props) {
    super();
    const defaultExercise = {
      title: "enter a title here",
      description: "and a short description",
      duration: 10,
      repetitions: 5,
      getSet: 3,
      id: props.exerciseID, // FIXME: exercises IDs are now managed by the db
    };
    if (typeof props.exercise === "undefined") this.state = defaultExercise;
    else this.state = { ...props.exercise };
    this.state = { ...this.state, modalID: props.modalID };
  }

  handleTextChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  changeInputValue = (variableName, e) => {
    const value = parseInt(e.target.value);
    e.preventDefault();
    if (value < 0) return;
    this.setState({ [variableName]: Math.round(value) });
  };

  //   returns a React object containing one number input and 2 buttons linked to cb and state properties
  getNumberInput(variableName, description) {
    return (
      <div className="col">
        <div className="text-center ">{description}</div>
        <div className="d-flex justify-content-center ">
          <div>
            <input
              type="number"
              className="form-control text-center my-3"
              value={this.state[variableName]}
              id={variableName}
              onChange={e => this.changeInputValue(variableName, e)}
              style={{ width: "5em" }}
            />
          </div>
        </div>
      </div>
    );
  }

  render() {
    const saveExercise = () => {
      const exercise = {
        title: this.state.title,
        description: this.state.description,
        duration: this.state.duration,
        repetitions: this.state.repetitions,
        getSet: this.state.repetitions,
        id: this.props.exerciseID,
      };
      this.props.saveExercise(exercise);
    };

    return (
      <div className="modal fade unselectable" tabIndex="-1" id={this.state.modalID}>
        <div className="modal-dialog modal-dialog-centered">
          <div className="modal-content">
            <div className="modal-body">
              <form>
                {/* title form */}
                <div className="mb-3">
                  <input
                    type="text"
                    className="form-control my-3"
                    id="title"
                    defaultValue={this.state.title}
                    onChange={this.handleTextChange}
                  />
                </div>
                {/* description form */}
                <div className="mb-3">
                  <textarea
                    className="form-control my-3"
                    id="description"
                    onChange={this.handleTextChange}
                    defaultValue={this.state.description}>
                    </textarea>
                </div>

                {/* number inputs : duration, repetitions and getset */}
                <div className="row align-items-center">
                  {this.getNumberInput("duration", "duration (in s)")}
                  {this.getNumberInput("repetitions", "repetitions")}
                  {this.getNumberInput("getSet", "get ready")}
                </div>
              </form>
            </div>
            <div className="my-3 mx-auto">
              <button type="button" className="btn btn-secondary mx-2" data-bs-dismiss="modal">
                Cancel
              </button>
              <button type="button" className="btn btn-primary mx-2" onClick={saveExercise} data-bs-dismiss="modal">
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewExerciseModal;
