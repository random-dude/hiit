import React, { Component } from "react";
import {
  BsPencilSquare,
  BsFillVolumeUpFill,
  BsFillVolumeMuteFill,
  BsTrash,
  BsPlusSquare,
  BsGear,
} from "react-icons/bs";

export class Navbar extends Component {
  state = { ...this.props };
  iconColor = "white";
  iconSize = 25;

  toggleSound = () => {
    this.setState(prevState => ({ isSoundOn: !prevState.isSoundOn }));
    this.props.toggleSound(); // callback to App.js
  };

  // returns <li> pills containing the exercise title for every exercise in database
  getNavLinks = () => {
    let exerciseList = [];
    for (let i = 0; i < this.props.database.length; i++) {
      const linkClass = i === this.props.selected ? "nav-link active" : "nav-link";
      exerciseList.push(
        <li className="nav-item exercise-name" key={i}>
          {/* eslint-disable-next-line */}
          <a href="#" className={linkClass} onClick={() => this.props.selectExercise(i)}>
            {this.props.database[i].title}
          </a>
        </li>
      );
    }
    return exerciseList;
  };

  render() {
    return (
      <div>
        <div className="d-flex justify-content-between">
          {/* exercises titles */}
          <ul className="nav nav-pills unselectable">{this.getNavLinks()}</ul>
          {/* sound icon */}
          <div className="mx-2 my-auto ms-auto unselectable" onClick={this.toggleSound}>
            {/* theses icons being smaller than the others, we upscale them by x1.2 */}
            {this.state.isSoundOn ? (
              <BsFillVolumeUpFill size={Math.round(this.iconSize * 1.2)} color={this.iconColor} />
            ) : (
              <BsFillVolumeMuteFill size={Math.round(this.iconSize * 1.2)} color={this.iconColor} />
            )}
          </div>
          {/* addExercise icon */}
          <div className="my-auto mx-2" data-bs-toggle="modal" data-bs-target="#addExerciseModal">
            <BsPlusSquare size={this.iconSize} color={this.iconColor} />
          </div>
          {/* editExercise icon */}
          <div className="my-auto mx-2" data-bs-toggle="modal" data-bs-target="#editExerciseModal">
            <BsPencilSquare size={this.iconSize} color={this.iconColor} />
          </div>
          {/* deleteExercise icon */}
          <div className="my-auto mx-2" onClick={() => this.props.deleteExercise(this.props.selected)}>
            <BsTrash size={this.iconSize} color={this.iconColor} />
          </div>
          {(this.props.isLoggedIn && (
            <React.Fragment>
              {/* user settings icon */}
              <div className="my-auto mx-2" data-bs-toggle="modal" data-bs-target="#userParametersModal">
                <BsGear size={this.iconSize} color={this.iconColor} />
              </div>
            </React.Fragment>
          )) || (
            <div className="my-auto mx-2" data-bs-toggle="modal" data-bs-target="#loginModal">
              <h6 className="my-auto unselectable">login/sign up</h6>
            </div>
          )}
        </div>
      </div>
    );
  }
}

//TODO : popover when there is to many items to display as pills (... pill with popover)
export default Navbar;
