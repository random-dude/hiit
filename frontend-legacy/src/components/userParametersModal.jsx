import React, { useState, useRef, useEffect } from "react";

export const UserParametersModal = props => {
  // ------------------------------- hooks -------------------------------

  // will toggle between default layout "", "passwordChange" and "deleteAccount"
  const [layout, setLayout] = useState("");
  // JSX content of the status text at the bottom of this modal
  const [statusText, setStatusText] = useState("");
  // refs of the inputs fields
  let inputsData = { currentPassword: useRef(null), newPassword: useRef(null), verifPassword: useRef(null) };
  // clear status text and inputs values on layout change
  useEffect(() => {
    if (layout === "") setStatusText("");
    Object.entries(inputsData).forEach(i => {
      if (i.current) i.current.value = "";
    });
  }, [layout]);

  // close this modal on logout
  useEffect(() => {
    if (props.username === "anonymous") document.querySelector("#userParametersModal").click();
  }, [props.username]);

  // ----------------------------- functions -----------------------------

  // verify that all required fields are populated before calling the API
  const changePassword = () => {
    let status = []; // will contain all the error messages if any
    // check for empty inputs
    const oldPassword = inputsData.currentPassword.current.value;
    const newPassword = inputsData.newPassword.current.value;
    if (oldPassword === "") status.push(<p className="text-warning my-0 my-0">please enter your current password</p>);
    if (newPassword === "") status.push(<p className="text-warning my-0">please enter a new password</p>);
    if (inputsData.verifPassword.current.value === "")
      status.push(<p className="text-warning my-0">please verify the new password</p>);
    // check password verification
    else if (inputsData.verifPassword.current.value !== newPassword)
      status.push(<p className="text-danger my-0">the new passwords does not match</p>);
    // no errors, continuing...
    if (status.length === 0) {
      fetch(props.apiURL + "/updatePassword", {
        method: "POST",
        body: JSON.stringify({ oldPassword: oldPassword, newPassword: newPassword }),
        headers: { Authorization: "Bearer " + props.token, "Content-Type": "application/json" },
      }).then(response => {
        if (response.status === 200) {
          props.login(props.username, newPassword);
          setStatusText(
            <p>
              <b>password updated successfully !</b>
            </p>
          );
          // user old password doesn't match to DB
        } else if (response.status === 409)
          setStatusText(
            <p className="text-danger">unable to update password, please verify your current password above</p>
          );
        else setStatusText(<p className="text-danger">error updating password: {response.statusText}</p>);
      });
      // errors found, displaying them
    } else setStatusText(status);
  };

  // check the password input then call the API
  const deleteAccount = password => {
    if (inputsData.currentPassword.current.value === "")
      setStatusText(<p className="text-warning">please enter your password above</p>);
    else {
      fetch(props.apiURL + "/deleteUser", {
        method: "DELETE",
        body: JSON.stringify({ password: inputsData.currentPassword.current.value }),
        headers: { Authorization: "Bearer " + props.token, "Content-Type": "application/json" },
      })
        .then(response => response.json())
        .then(r => {
          if (!r) setStatusText(<p className="text-danger">unable to delete account : not found or bad password ?</p>);
          else {
            props.logout();
          }
        });
    }
  };

  // returns the markup for a password input with ref and text label
  const getPasswordInput = (label, ref) => (
    <div className="row">
      <label className="col-sm-5 col-form-label my-auto">{label}</label>
      <div className="col-sm-7 my-auto">
        <input
          type="password"
          className="form-control mb-2"
          id={label.replaceAll(" ", "-")}
          ref={ref}
          key={label}
          defaultValue=""
          required
        />
      </div>
    </div>
  );

  // ----------------------------- Markup -----------------------------
  return (
    <div className="modal fade unselectable text-center" tabIndex="-1" id="userParametersModal">
      <div className="modal-dialog modal-dialog-centered mx-auto modal-sm">
        <div className="modal-content">
          <div className="modal-header mx-auto">
            <h5>user settings for {props.username}</h5>
          </div>
          <div className="modal-body">
            {/* logout button */}
            <div>
              <button type="button" className="btn btn-secondary my-3" data-bs-dismiss="modal" onClick={props.logout}>
                log out
              </button>
            </div>

            {/* change password button */}
            <div>
              <button type="button" className="btn btn-secondary my-3" onClick={() => setLayout("passwordChange")}>
                change password
              </button>
            </div>

            {/* password change form */}
            {layout === "passwordChange" && (
              <React.Fragment>
                <form>
                  {/* current password input */}
                  {getPasswordInput("current password", inputsData.currentPassword)}
                  {/* new password */}
                  {getPasswordInput("new password", inputsData.newPassword)}
                  {/* password verification */}
                  {getPasswordInput("repeat new password", inputsData.verifPassword)}
                </form>
                <div>
                  <button type="button" className="btn btn-primary my-1" onClick={changePassword}>
                    confirm
                  </button>
                </div>
              </React.Fragment>
            )}

            {/* delete account button */}
            <div>
              <button type="button" className="btn btn-warning my-3" onClick={() => setLayout("deleteAccount")}>
                delete account
              </button>
            </div>
            {/* delete account confirmation text and button */}
            {layout === "deleteAccount" && (
              <React.Fragment>
                <div className="row text-danger">
                  Deleting this account will remove every exercise. <b>They will be forever lost.</b>
                </div>
                <form>{getPasswordInput("current password", inputsData.currentPassword)}</form>
                <div>
                  <button type="button" className="btn btn-danger my-3" onClick={deleteAccount}>
                    delete anyway
                  </button>
                </div>
              </React.Fragment>
            )}

            {/* status text and spinner */}
            {statusText && <div className="row">{statusText}</div>}

            {/* exit button */}
            <button
              type="button"
              className="btn btn-secondary my-3"
              data-bs-dismiss="modal"
              onClick={() => setLayout("")}>
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserParametersModal;
