import React, { Component } from "react";
// interesting alternative here;
// https://stackoverflow.com/questions/47616619/is-there-a-way-to-play-a-music-with-audio-in-react/47616905
const audioFolder = process.env.PUBLIC_URL + "/audio/";

export class PlayAudio extends Component {
  audioElements = {};

  constructor(props) {
    super();
    for (let key of ["getSetAudio", "goAudio", "restAudio", "congratsAudio"]) {
      this.audioElements[key] = new Audio(audioFolder + props.config[key]);
    }
  }

  play = key => {
    if (key in this.audioElements) this.audioElements[key].play();
    else console.log("audio for", key, "not found in config:", this.props.config);
  };

  componentWillUnmount() {
    for (let key in this.audioElements) {
      this.audioElements[key].pause();
      this.audioElements[key].currentTime  =  0;
    }
  }

  render() {
    return <div></div>;
  }
}

export default PlayAudio;
