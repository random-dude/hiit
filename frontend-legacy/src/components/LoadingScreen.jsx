import React from "react";
import "../spinner.min.css";

export default function LoadingScreen() {
  return (
    <div className="jumbotron text-center" style={{ marginTop: "15%" }}>
      <h1>loading database</h1>
      <div className="lds-spinner my-4">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>
  );
}
