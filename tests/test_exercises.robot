*** Settings ***
Library          SeleniumLibrary
Library          OperatingSystem
Library          Process
Library          RequestsLibrary
Library          robot_utils.py
Resource         global_utils.robot
Suite Setup      Setup
Suite Teardown   Teardown

*** Variables ***

${Test Username}           ___test_user___
${Test Password}           ___test_password___ 
&{Short Exercise}          title=testExercise   description=this is a description  duration=${6}  repetitions=${2}  getSet=${2}
# a sluggish browser may offset the timing a bit causing the test to fail, hence the need for a tolerance in measurements
${tolerance}       500ms


*** Tasks ***

Add a short test exercise                        Add Exercise          ${Short Exercise}
Select test exercise                             Select Exercise       ${Short Exercise}[title]
Verify that test exercise is correctly loaded    Exercise is Loaded    ${Short Exercise}
Test the exercise itself                         Try Exercise          ${Short Exercise}


*** Keywords ***

Setup
    Launch Hiit
    Sign up User    ${Test Username}     ${Test Password}

Teardown
    Remove User From DB     ${Test Username}
    Close Hiit

Try Exercise
    [Arguments]          ${exercise}
    ${rest duration}=    Get Rest Duration                ${exercise}
    Click Element        css:.CircularProgressbar-text
    FOR    ${i}    IN RANGE    ${exercise["repetitions"]}
        IF    ${exercise["getSet"]} != 0
            Wait Until Element Contains    css:.CircularProgressbar-text    get ready    timeout=${tolerance}
            Sleep                          ${exercise["getSet"]}s
            Wait Until Element Contains    css:.CircularProgressbar-text    work         timeout=${tolerance}
        ELSE
            Wait Until Element Contains    css:.CircularProgressbar-text    work         timeout=${tolerance}
        END
        Sleep    ${exercise["duration"]}s
        IF    ${i} < ${exercise["repetitions"] - 1}
            Wait Until Element Contains   css:.CircularProgressbar-text    rest  timeout=${tolerance}
            Sleep                         ${rest duration}s
        END
    END
    Wait Until Element Contains    css:.CircularProgressbar-text    well done    timeout=${tolerance}

