import warnings
import db

TEST_USERNAME = "__test_username__"
TEST_PASSWORD = "password"
CONFIGURATION_TYPES = {"user":str, "isSoundOn":bool, "timeIncrement":int, "getSetAudio":str, 
                          "goAudio":str, "restAudio":str, "congratsAudio":str }
EXERCISES_TYPES = {"user":str, "id":int, "title":str, "description":str, "duration":int, 
                     "repetitions":int, "getSet":int}

database = db.Database()

def validateDict(types:dict, dictionnary:dict, dictName:str, referenceDict=None):
    """ make assertions on the composition of a given dict. 
    Verify the presence of keys and the type of associated values

    Args:
        types (dict): dict describing the types to assert {"myString":str, "myInt":int,...}
        dictionnary (dict): the dict to be validated
        dictName (str): name of the dict (to be printed on stdout)
        referenceDict (dict/None, optional): any couple of key/value will be verified to be the dict. Defaults to None.
    """
    for k, t in types.items() :
        print(f"  asserting {k} exists in {dictName}")
        assert k in dictionnary
        print(f"  asserting {k} is of {t}")
        assert isinstance(dictionnary[k], t)
        if referenceDict and k in referenceDict : 
            assert dictionnary[k] == referenceDict[k]

def test_connect_to_mySQL():
    assert database.getConnection() is True

def test_add_user():
    if database.userExists(TEST_USERNAME):
        warnings.warn("test user already exists, wasn't it deleted during a previous test run ?")
        database.deleteUser(TEST_USERNAME)
    assert database.userExists(TEST_USERNAME) is False
    database.addUser(TEST_USERNAME, TEST_PASSWORD)
    assert database.userExists(TEST_USERNAME)

def test_user_salted_hash():
    saltedHash = database.getSaltedHash(TEST_USERNAME)
    assert db.config.API.password.verify(TEST_PASSWORD, saltedHash)

def test_change_password():
    reversedPassword = TEST_PASSWORD [::-1]
    previousHash = database.getSaltedHash(TEST_USERNAME)
    database.updatePassword(TEST_USERNAME, reversedPassword)
    currentHash = database.getSaltedHash(TEST_USERNAME)
    assert currentHash != previousHash
    assert db.config.API.password.verify(reversedPassword, previousHash) is False
    assert db.config.API.password.verify(reversedPassword, currentHash)

def test_new_user_configuration():
    configuration = database.getConfiguration(TEST_USERNAME)
    assert len(configuration) > 0
    validateDict(CONFIGURATION_TYPES, configuration, "configuration", {"user":TEST_USERNAME})

def test_update_configuration():
    newConfig = {"isSoundOn":False, "timeIncrement":42, "getSetAudio":"getSet!", 
                 "goAudio":"go!", "restAudio":"oof", "congratsAudio":"yay!" }
    for k, v in newConfig.items():
        database.setConfiguration(k, v, TEST_USERNAME)
    # the user key is automatically added by db.py
    newConfig.update({"user":TEST_USERNAME})
    assert database.getConfiguration(TEST_USERNAME) == newConfig

def test_new_user_exercises():
    exercises = database.getAllExercises(TEST_USERNAME)
    assert len(exercises) > 0
    validateDict(EXERCISES_TYPES, exercises[0], "exercise", {"user":TEST_USERNAME})

def test_add_exercise():
    exercise = {"title":"blah", "description":"blah x12", "duration":42, 
                "repetitions":4242, "getSet":424242}
    database.addExercise(exercise, TEST_USERNAME)
    newExercise = database.getAllExercises(TEST_USERNAME)[-1]
    exercise.update({"id":newExercise["id"], "user":TEST_USERNAME})
    assert newExercise == exercise

def test_update_exercise():
    exercise = database.getAllExercises(TEST_USERNAME)[-1]
    exercise["title"] = "this has been updated"
    exercise["description"] = "and this as well"
    database.updateExercise(exercise, TEST_USERNAME)
    assert database.getAllExercises(TEST_USERNAME)[-1] == exercise

def test_remove_exercise():
    exercises = database.getAllExercises(TEST_USERNAME)
    exercisesIDs = [e["id"] for e in exercises]
    database.removeExercise(exercisesIDs.pop(), TEST_USERNAME)
    assert database.getAllExercises(TEST_USERNAME) == exercises[:-1]

def test_delete_user():
    database.deleteUser(TEST_USERNAME)
    assert database.userExists(TEST_USERNAME) is False
    assert database.getAllExercises(TEST_USERNAME) == []
    assert database.getConfiguration(TEST_USERNAME) == []
