*** Settings ***
Library          SeleniumLibrary
Library          OperatingSystem
Library          Process
Library          RequestsLibrary

*** Variables ***
${Browser}                 firefox

*** Keywords ***

Setup Selenium
    Create Directory            .selenium-screenshots
    Set Screenshot Directory    .selenium-screenshots
    # Set Selenium Speed          100ms

Run Debug Server
    ${Environment}=             Get Environment Variables
    Set Suite Variable          ${Main Page URL}            http://${Environment}[HTTP_HOST]:${Environment}[HTTP_PORT]
    Set Suite Variable          ${API URL}                  http://${Environment}[API_HOST]:${Environment}[API_PORT]
    Start Process                  ./run    --debug    shell=true    cwd=../

Frontend is Running
    GET  ${Main Page URL}
    Status Should Be  200

API is Running
    GET  ${API URL}
    Status Should Be  200

Open Main Page
    Open Browser                ${Main Page URL}    ${Browser}
    Maximize Browser Window
    Wait Until Page Contains    click to start

Launch Hiit
    Setup Selenium
    Run Debug Server
    Wait Until Keyword Succeeds    30    1s    Frontend is Running
    Wait Until Keyword Succeeds    30    1s    API is Running
    Open Main Page

Teardown Selenium
    Close Browser
    Remove Directory    .selenium-screenshots    recursive=True
    Remove Files        logs/geckodriver-*.log

Teardown Debug Server
    Terminate All Processes

Close Hiit
    Teardown Selenium
    Teardown Debug Server

Wait Until Modal Closes
    Wait Until Page Does Not Contain Element    css:div.modal-backdrop.show
    Sleep    100ms

Sign up User
    [Arguments]                 ${username}          ${password}
    Click Element               xpath://h6[text()='login/sign up']
    Click Button                Signup
    Input Text                  css:input#username   ${username}
    Input Text                  css:input#password   ${password}
    Input Text                  css:input#confirm    ${password}
    ${Captcha Text}             Get Text             css:.modal-content .row p
    ${Captcha Answer}           Solve Captcha        ${Captcha Text}
    Input Text                  css:input#captcha    ${Captcha Answer}
    Click Button                Signup
    Wait Until Page Contains    successfully signed up and logged in !
    Click Button                Close
    Wait Until Modal Closes    

Add Exercise
    [Arguments]    ${exercise}
    Click Element                        css:div[data-bs-target="#addExerciseModal"]
    Wait Until Page Contains Element     css:input#title
    Input Text                           css:input#title             ${exercise["title"]}
    Input Text                           css:textarea#description    ${exercise["description"]}
    Input Text                           css:input#duration          ${exercise["duration"]}
    Input Text                           css:input#repetitions       ${exercise["repetitions"]}
    Input Text                           css:input#getSet            ${exercise["getSet"]}
    Click Button                         Save
    Wait Until Modal Closes
    Page Should Contain                  ${exercise["title"]}       

Select Exercise
    [Arguments]                    ${title}
    Page Should Contain Element    xpath://a[text()="${title}"]
    Click Element                  xpath://a[text()="${title}"]
    Wait Until Element Contains    css:h1    ${title}

Exercise is Loaded
    [Arguments]               ${exercise}
    Element Should Contain    css:h1        ${exercise["title"]}
    Element Should Contain    css:p.mx-5    ${exercise["description"]}
    # Element Should Contain    css:p    total duration 
    Element Should Contain    css:text      click to start
