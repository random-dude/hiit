from db import db

def solve_captcha(captchaText:str) -> str:
    """ calculate the requested sum needed to allow for user signup

    Args:
        captchaText (str): the text of the captcha, as displayed for the user

    Returns:
        str: the sum ready to enter in the <input>
    """
    captchaText = captchaText.split(" dumbbells")
    firstNumber = captchaText[0].split(" ")[-1]
    secondNumber = captchaText[1].split(" ")[-1]
    return str(int(firstNumber)+int(secondNumber))

def remove_user_from_db(username):
    db.deleteUser(username)

def user_exists(username:str):
    assert db.userExists(username)

def user_does_not_exists(username:str):
    assert db.userExists(username) is False

def get_rest_duration(exercise:dict) -> int:
    return round(exercise["duration"]/2) - exercise["getSet"]

# def get_total_duration_string(exercise):
#     rest = max(int(exercise["duration"]/2)-exercise["getSet"], 0)
#     duration = exercise["repetitions"] * (exercise["duration"] + exercise["getSet"] + rest) - rest
#     # rest = round(exercise["duration"]/2)
#     # duration = exercise["duration"]*exercise["repetitions"] + rest*(exercise["repetitions"]-1)
#     minutes, seconds = int(duration/60), duration%60
#     return f"total duration {minutes:02}m{seconds:02}s"

# get_total_duration_string(exercise)
