# unit tests
The unit tests are written with **pytest**. They covers nearly 100% of the API features. Every method of *db.py* and every route from *app.py* are tested, both with and without authentication.
- **test_API** checks the routes
- **test_database** verify methods of the *Database* class

 It may seems redundant to test the same function directly and using http requests, although failing both tests will clearly indicate a DB error while failing only the requests side points to an API/frontend malfunction.

# functional tests
Functional testing is carried out end-to-end by **RobotFramework** and **selenium**. Most features are also covered from the user standpoint.
- **test_users.robots** tests the authentication related features : logging in and out, signing up, re-logging, password change...
- **test_exercises.robots** tests the creation, modification and deletion of exercises, as well as the exercise itself
- global_utils.robot provides global keywords used in setup and teardown for instance. It is used as a *Resource* by the suites.
- robot_utils.py contains short python utilities to solve captcha and communicate directly with the database. It is used as a library by *global_utils.robot*

# running the tests
The provided **runTest** script sets the environnement up by assigning env variables and alters the *PYTHON_PATH* to allow pytest to load modules from the parent *backend* folder. It will then launch both the unitary and functional tests. All logs are stored in the *logs* subfolder.

The provided **setup** script for this project doesn't include the installation of *pytest*, *RobotFramework* and libraries used for testing to avoid bloating a deployment server. They must be installed separately :
```bash
# install the tests libraries
pip install -U pytest robotframework robotframework-seleniumlibrary robotframework-requests
# webdrivermanager will automatically download drivers for the installed version of the specified browser
pip install -U webdrivermanager
# linking the drivers binaries may be omitted if you add their location to the $PATH
webdrivermanager firefox chrome --linkpath /usr/local/bin
```