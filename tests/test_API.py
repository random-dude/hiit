# import app
from fastapi.testclient import TestClient
import pytest
from app import app
import defaults

TEST_USERNAME = "__test_username__"
TEST_PASSWORD = "password"

client = TestClient(app)
token = None

# get exercises without providing any Authorization in the headers
def test_public_get_exercises():
    response = client.get("/api/getExercises")
    assert response.status_code == 200
    exercises = response.json()
    assert len(exercises) > 0
    assert exercises == defaults.database
    
# same thing with configuration
def test_public_get_configuration():
    response = client.get("/api/config")
    assert response.status_code == 200
    configuration = response.json()
    assert configuration == defaults.config

def test_private_routes_are_privates():
    """all theses routes should returns 401 if requested without a valid JWT in the Authorization header
    """
    privateRoutes = {"updatePassword":client.post, "update":client.post, "add":client.post, 
                     "remove0":client.delete, "deleteUser":client.delete}
    for url, method in privateRoutes.items():
        response = method("/api/"+url)
        assert response.status_code == 401

def test_signup_user():
    response = client.post("/api/signup", json={"username":TEST_USERNAME, "password":TEST_PASSWORD})
    if response.status_code == 409 : pytest.fail("test user already exists (hasn't been deleted from previous test run ?)")
    assert response.status_code == 201

def test_login():
    global token
    response = client.post("/api/login", json={"username":TEST_USERNAME, "password": TEST_PASSWORD})
    assert response.status_code == 200
    token = "Bearer "+response.json()
    # wrong username, same password
    response = client.post("/api/login", json={"username":TEST_USERNAME[::-1], "password": TEST_PASSWORD})
    assert response.status_code == 401
    # wrong password, valid username
    response = client.post("/api/login", json={"username":TEST_USERNAME, "password": TEST_PASSWORD[::-1]})
    assert response.status_code == 401

def test_get_user_configuration():
    response = client.get("/api/config", headers={"Authorization":token})
    assert response.status_code == 200
    configuration = response.json()
    for k in ("user", "isSoundOn", "timeIncrement", "getSetAudio", "goAudio", "restAudio", "congratsAudio"):
        assert k in configuration
    assert configuration["user"] == TEST_USERNAME

def test_update_user_configuration():
    newConfig = {"isSoundOn":False, "timeIncrement":42, "getSetAudio":"getSet!", 
                 "goAudio":"go!", "restAudio":"oof", "congratsAudio":"yay!" }
    response = client.post("/api/config", json={**newConfig}, headers={"Authorization":token})
    assert response.status_code == 200
    newConfig.update({"user":TEST_USERNAME})
    assert response.json() == newConfig

def test_get_user_exercises():
    response = client.get("/api/getExercises", headers={"Authorization":token})
    assert response.status_code == 200
    exercises = response.json()
    assert len(exercises) > 0
    for k in ("user","title", "description", "duration", "repetitions", "getSet", "id"):
        assert k in exercises[0]
    assert exercises[0]["user"] == TEST_USERNAME

def test_update_user_exercises():
    exercise = client.get("/api/getExercises", headers={"Authorization":token}).json()[0]
    exercise.update({"title":"updated title", "description":"updated description", "duration":42})
    response = client.post("/api/update", json={**exercise}, headers={"Authorization":token})
    assert response.status_code == 200
    assert exercise == client.get("/api/getExercises", headers={"Authorization":token}).json()[0]

def test_add_exercise():
    exercise = {"title":"blah", "description":"blah x12", "duration":42, 
                "repetitions":4242, "getSet":424242}
    response = client.post("/api/add", json={**exercise}, headers={"Authorization":token})
    assert response.status_code == 200
    exercises = client.get("/api/getExercises", headers={"Authorization":token}).json()
    # user and id have been automatically added
    exercise.update({"user":TEST_USERNAME, "id":exercises[-1]["id"]})
    assert exercise == exercises[-1]

def test_remove_exercise():
    exercises = client.get("/api/getExercises", headers={"Authorization":token}).json()
    response = client.delete(f'/api/remove{exercises[-1]["id"]}', headers={"Authorization":token})
    assert response.status_code == 200
    afterDeletion = client.get("/api/getExercises", headers={"Authorization":token}).json()
    assert afterDeletion == exercises[:-1]

def test_update_password():
    response = client.post("/api/updatePassword", json={"oldPassword":TEST_PASSWORD, "newPassword":TEST_PASSWORD[::-1]},
                           headers={"Authorization":token})
    assert response.status_code == 200
    # the old password doesn't work anymore
    response = client.post("/api/login", json={"username":TEST_USERNAME, "password": TEST_PASSWORD})
    assert response.status_code == 401
    # but the new one does
    response = client.post("/api/login", json={"username":TEST_USERNAME, "password": TEST_PASSWORD[::-1]})
    assert response.status_code == 200

def test_remove_user():
    response = client.delete("/api/deleteUser", json={"password":TEST_PASSWORD[::-1]}, headers={"Authorization":token})
    assert response.status_code == 200
    assert response.json() == True
