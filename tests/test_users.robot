*** Settings ***
Library          SeleniumLibrary
Library          OperatingSystem
Library          Process
Library          RequestsLibrary
Library          robot_utils.py
Resource         global_utils.robot
Suite Setup      Setup
Suite Teardown   Teardown

*** Variables ***
${Browser}                 firefox
${Test Username}           ___test_user___
${Test Password}           ___test_password___ 


*** Tasks ***

Sign up Test User                 Sign up User    ${Test Username}    ${Test Password} 
User is in Database               User exists    ${Test Username}
Autologin on Sign up              is Logged In
User Stay Logged In via Cookies   Reload Page
                                  is Logged In
Log User Out                      Log User Out
                                #   Sleep    60s
Log User In                       Log User In    ${Test Username}    ${Test Password}
Change User Password              Change Password    ${Test Password}    horriblePassword
Delete Test User                  Delete User    horriblePassword
Test User doesn't exist in DB     User Does Not Exists    ${Test Username}


*** Keywords ***
Setup
    Launch Hiit
    Delete All Cookies

Teardown
    Remove User From DB     ${Test Username}
    Close Hiit

Sign up User
    [Arguments]                 ${username}          ${password}
    is Logged Out
    Click Element               xpath://h6[text()='login/sign up']
    Page Should Contain         login / signup
    Click Button                Signup
    Page Should Contain         username
    Page Should Contain         password
    Page Should Contain         confirm
    Page Should Contain         answer
    Input Text                  css:input#username   ${username}
    Input Text                  css:input#password   ${password}
    Input Text                  css:input#confirm    ${password}
    ${Captcha Text}             Get Text             css:.modal-content .row p
    ${Captcha Answer}           Solve Captcha        ${Captcha Text}
    Input Text                  css:input#captcha    ${Captcha Answer}
    Click Button                Signup
    Wait Until Page Contains    successfully signed up and logged in !
    # reset the modal layout for future logins
    Click Button                Login
    Click Button                Close
    Wait Until Modal Closes
    is Logged In
    # Wait Until Keyword Succeeds    10    50ms    is Logged In
    
is Logged In
    Page Should Contain Element    css:div[data-bs-target="#userParametersModal"]
    Page Should Not Contain        login/sign up


is Logged Out
    Page Should Contain                login/sign up
    Page Should Not Contain Element    css:div[data-bs-target="#userParametersModal"]

Delete User
    [Arguments]                   ${password}
    is Logged In
    Open User Settings
    Click Button                   delete account
    Page Should Contain Element    css:input#current-password
    Input Text                     css:input#current-password   ${password}
    Click Button                   delete anyway
    Wait Until Modal Closes
    is Logged Out

Change Password
    [Arguments]     ${current password}              ${new password}
    is Logged In
    Open User Settings
    Click Button                change password
    Input Text                  css:input#current-password       ${current password}
    Input Text                  css:input#new-password           ${new password}
    Input Text                  css:input#repeat-new-password    ${new password}
    Click Button                xpath://button[text()='confirm']
    Wait Until Page Contains    password updated successfully !
    # Close modal button
    Click Button                css:button.btn-secondary.my-3
    Wait Until Modal Closes

Log User In
    [Arguments]                 ${username}          ${password}
    is Logged Out
    Click Element               xpath://h6[text()='login/sign up']
    Page Should Contain         login / signup
    Input Text                  css:input#username    ${username}
    Input Text                  css:input#password    ${password}
    Click Button                Login
    Wait Until Modal Closes
    is Logged In
    
Log User Out
    is Logged In
    Open User Settings
    # the modal needs time to be updated
    Sleep                         500ms
    Click Button                  xpath://button[text()='log out']
    Wait Until Modal Closes
    is Logged Out

Open User Settings
    Click Element                       css:div[data-bs-target="#userParametersModal"]
    Page Should Contain                 user settings for 
    Wait Until Page Contains Element    css:div#userParametersModal.show
