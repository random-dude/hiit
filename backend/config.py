import os


class DB:
    host = os.getenv("DB_HOST", "localhost")
    user = os.getenv("DB_USER", "root")
    password = os.getenv("DB_PASSWORD", "password")
    database = os.getenv("DB_NAME", "hiit")


class API:
    host = os.getenv("API_HOST", "localhost")
    HTTPhost = os.getenv("HTTP_HOST", "localhost")
    port = int(os.getenv("API_PORT", "3000"))
    HTTPport = int(os.getenv("HTTP_PORT", "80"))
    origins = [
        f"https://{HTTPhost}:{HTTPport}",
        f"https://{HTTPhost}",
        f"http://{HTTPhost}:{HTTPport}",
        f"http://{HTTPhost}",
        f"{HTTPhost}:{HTTPport}",
        f"{HTTPhost}",
    ]
