from passlib.context import CryptContext

password = CryptContext(schemes=["bcrypt"], deprecated="auto")


class Default:
    exercise = {
        "title": "dummy",
        "description": "For debug purposes, not much calories burnt on this one :S",
        "repetitions": 2,
        "duration": 2,
        "getSet": 0,
    }

    configuration = {
        "isSoundOn": True,
        "timeIncrement": 150,
        "getSetAudio": "Final Fantasy VII Sound Effects - Cursor Move - YouTube.mp3",
        "goAudio": "MGS-Alert.mp3",
        "restAudio": "Final Fantasy VII Sound Effects - Enter Save - YouTube.mp3",
        "congratsAudio": "FFVII-victory.mp3",
    }

    user = {
        "username": "public",
        "password": password.hash("password"),
        "exercisesID": [],
        "configuration": configuration,
    }
