db = db.getSiblingDB(process.env.DB_NAME || "hiit");

db.createUser({
  user: process.env.DB_USER || "root",
  pwd: process.env.DB_PASSWORD || "password",
  roles: [
    {
      role: "readWrite",
      db: process.env.DB_USER || "root",
    },
  ],
});

//  TODO: bootstrap DB with demo exercises, demo user, default configuration...
// db.createCollection("");
// db.got_seasons_collection.insertMany([{}]);
