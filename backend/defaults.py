database = [
    {
        "title": "edit me",
        "description": "click on the pencil icon above to edit this exercise and make it yours",
        "repetitions": 2,
        "duration": 2,
        "getSet": 0,
        "id": 0,
        "user": "anonymous",
    },
    {
        "title": "delete me",
        "description": "click on the pencil icon above to edit this exercise and make it yours",
        "repetitions": 2,
        "duration": 2,
        "getSet": 0,
        "id": 1,
        "user": "anonymous",
    },
]

config = {
    "user": "anonymous",
    "isSoundOn": True,
    "timeIncrement": 150,
    "getSetAudio": "Final Fantasy VII Sound Effects - Cursor Move - YouTube.mp3",
    "goAudio": "MGS-Alert.mp3",
    "restAudio": "Final Fantasy VII Sound Effects - Enter Save - YouTube.mp3",
    "congratsAudio": "FFVII-victory.mp3",
}
