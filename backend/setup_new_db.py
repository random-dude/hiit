from defaults_values import Default


def init_database(database):
    """
    Creates the initial hiit database with the user collection and a default user document
    """
    if "users" in database.list_collection_names() and database["users"].find_one(
        {"username": "public"}
    ):
        print("  public user already exists, skipping DB initialisation")
        return
    else:
        print("  no public user found, initialising DB...")
        defaultExercise = database["exercises"].insert_one(Default.exercise)
        defaultUser = Default.user
        defaultUser["exercisesID"].append(defaultExercise.inserted_id)
        users = database["users"]
        users.create_index("username", unique=True)
        defaultUser = users.insert_one(Default.user)
