from fastapi import APIRouter, HTTPException, Depends

from users.models import *
from users.services import *
from login.models import JWTuser, LoginResponse, ChangePassword
from login.views import login, setJWTtokenCookie

router = APIRouter()


@router.delete("/user", status_code=204)
async def deleteUser(password: str, user: JWTuser = Depends(login)):
    deleted = await removeUser(user, password)
    if not deleted:
        raise HTTPException(
            status_code=403, detail=f"unable to delete user {user.username}"
        )


@router.patch("/user", response_model=LoginResponse)
async def updatePassword(password: ChangePassword, user: JWTuser = Depends(login)):
    updatedUser = await passwordUpdate(user, password)
    if not updatedUser:
        raise HTTPException(
            status_code=403, detail=f"unable to update user {user.username}'s password"
        )
    return setJWTtokenCookie(updatedUser, login)


@router.get("/configuration", response_model=Configuration)
async def get_configuration(user: JWTuser = Depends(login)):
    return await getConfiguration(user)


@router.patch("/configuration", response_model=Configuration)
async def patch_configuration(
    parameters: PatchConfiguration, user: JWTuser = Depends(login)
):
    return await patchConfiguration(user, parameters)
