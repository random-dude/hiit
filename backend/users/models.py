from typing import List
from pydantic import BaseModel
from beanie import Document, Indexed, Link
from pydantic_partial import create_partial_model

from exercises.models import Exercice

defaultConfiguration = {
    "isSoundOn": True,
    "timeIncrement": 150,
    "getSetAudio": "Final Fantasy VII Sound Effects - Cursor Move - YouTube.mp3",
    "goAudio": "MGS-Alert.mp3",
    "restAudio": "Final Fantasy VII Sound Effects - Enter Save - YouTube.mp3",
    "congratsAudio": "FFVII-victory.mp3",
}


class Configuration(BaseModel):
    isSoundOn: bool
    timeIncrement: int
    getSetAudio: str
    goAudio: str
    restAudio: str
    congratsAudio: str


PatchConfiguration = create_partial_model(Configuration)


class User(Document):
    username: Indexed(str, unique=True)
    password: str
    configuration: Configuration = defaultConfiguration
    exercises: List[Link[Exercice]] = []

    class Settings:
        name = "users"


class UserExercises(BaseModel):
    exercises: List[Exercice]


class UserConfiguration(BaseModel):
    configuration: Configuration
