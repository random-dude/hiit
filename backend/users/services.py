from beanie import DeleteRules

from users.models import *
from login.models import JWTuser
from login.services import passwd


async def addUser(username: str, password: str) -> User:
    """add user and salted hash password to the users table, create defaults entries in configuration and exercises tables.

    Args:
        username (str): username
        password (str): non-hashed password

    Returns:
        [bool]: True if succeeded, False if user already exists
    """
    user = await User.find_one(User.username == username, fetch_links=False)
    if user:
        return None
    else:
        password = passwd.hash(password)
        user = User(username=username, password=password)
        print(f"  inserting user {user}")
        await user.create()
        return user


async def removeUser(user: JWTuser, password: str) -> bool | None:
    """remove user data from all the tables. Returns silently if the user doesn't exists

    Args:
        password (str): current password for protection against cookie theft
    """
    user = await User.find_one(User.username == user.username, fetch_links=True)
    if user and passwd.verify(password, user.password):
        user = await user.delete(link_rule=DeleteRules.DELETE_LINKS)
        return True
    return None


async def passwordUpdate(user: JWTuser, password: str) -> User | None:
    """changes the user password

    Args:
        username (str): username
        password (str): clear text password
    """
    user = await User.find_one(User.username == user.username, fetch_links=False)
    if user and passwd.verify(password.current, user.password):
        user.password = passwd.hash(password.new)
        return await user.save()
    else:
        return None


async def patchConfiguration(
    user: JWTuser, parameters: PatchConfiguration
) -> Configuration:
    user = await User.find_one(User.username == user.username, fetch_links=False)
    configuration = user.configuration.dict()
    configuration.update(parameters.dict(exclude_unset=True))
    user = await user.set({"configuration": Configuration(**configuration)})
    return user.configuration


async def getConfiguration(user: JWTuser) -> Configuration:
    user = await User.find_one(
        User.username == user.username, fetch_links=False
    ).project(UserConfiguration)
    return user.configuration
