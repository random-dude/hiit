import os
from fastapi import APIRouter, HTTPException
from fastapi_login import LoginManager
from fastapi_login.exceptions import InvalidCredentialsException
from fastapi.responses import JSONResponse
from datetime import timedelta

from login.models import *
from login.services import *
from users.services import addUser

SECRET = bytes(os.environ["SECRET_KEY"], "utf-8")
login = LoginManager(SECRET, "/api/login", use_cookie=True)
router = APIRouter()


@login.user_loader()
async def get_user(username: str):
    jwtUser = await getByUsername(username)
    return jwtUser
    # FIXME : handle the public case where everyone share the default user


# login.useRequest(app) # needed to use the same routes when logged in or out


@router.post("/login", response_model=LoginResponse)
async def userLogin(creds: Credentials):
    """tries to log the user in and generate a json web token

    Args:
        creds (dict): {username:str, password:str}

    Raises:
        InvalidCredentialsException: if the user is not found or if the password doesn't verify against the salted hash

    Returns:
        creds (dict): {username: str, id: str}
        and sets the JWT token cookie in the response
    """
    user = await logUserIn(creds.username, creds.password)
    if user:
        return setJWTtokenCookie(user)
    else:
        raise InvalidCredentialsException


@router.put("/signup", response_model=LoginResponse)
async def signup(creds: Credentials):
    user = await addUser(creds.username, creds.password)
    if user:
        return setJWTtokenCookie(user, login)
    else:
        raise HTTPException(status_code=409, detail="the username isn't available")


def setJWTtokenCookie(user, login):
    token = login.create_access_token(
        data={"sub": user.username, "id": str(user.id)}, expires=timedelta(hours=48)
    )
    response = JSONResponse({"username": user.username, "id": str(user.id)})
    login.set_cookie(response, token)
    return response
