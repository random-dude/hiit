from passlib.context import CryptContext
from login.models import *
from users.models import User

passwd = CryptContext(schemes=["bcrypt"], deprecated="auto")


async def logUserIn(username: str, password: str) -> User | None:
    """tries to log the user in and generate a json web token
    Args:
        data (dict): {username:str, password:str}

    Raises:
        InvalidCredentialsException: if the user is not found or if the password doesn't verify against the salted hash

    Returns:
        str: json web token
    """
    user = await User.find_one(User.username == username, fetch_links=False)
    print("user", user, "username", username, "pass", password)
    return user if user and passwd.verify(password, user.password) else None


def getByUsername(username: str, selects={}) -> User | None:
    return User.find_one(User.username == username, fetch_links=False).project(JWTuser)
