from pydantic import BaseModel


class JWTuser(BaseModel):
    username: str


class Credentials(BaseModel):
    username: str
    password: str


class LoginResponse(BaseModel):
    username: str
    id: str


class ChangePassword(BaseModel):
    current: str
    new: str
