import config


class Database:
    configuration = {
        "host": config.DB.host,
        "user": config.DB.user,
        "password": config.DB.password,
        "database": config.DB.database,
    }

    def __init__(self):
        pass

    def getSaltedHash(self, username):
        """returns the salted hash of the password for a given user

        Args:
            username (str): user name

        Returns:
            str: salted hash by the algorithm defined in config.py
        """
        return self.getByUsername(username, {"password": 1})


db = Database()


def readJSON(file):
    import json

    try:
        data = json.load(open(file))
        return data
    except Exception as e:
        print(e)


def addExercisesFromJSON(file="exercises.json"):
    exercises = readJSON(file)
    if exercises:
        for exercise in exercises:
            db.addExercise(exercise, None)
        print(f"loaded {len(exercises)} exercise(s) from JSON file {file}")
    else:
        print("failed loading exercises from JSON file ", file)


def importConfigFromJSON(file="config.json"):
    configuration = readJSON(file)
    if configuration:
        for k, v in configuration.items():
            db.setConfiguration(k, v, "public")
        print("restored configuration from file", file)
    else:
        print("failed loading configuration from JSON file", file)
