from pydantic_partial import create_partial_model
from pydantic import BaseModel
from beanie import Document


class Exercice(Document):
    _id: str
    title: str
    description: str
    repetitions: int
    duration: int
    getSet: int
    intensity: int = 0

    class Settings:
        name = "exercises"


class ExerciceDTO(BaseModel):
    title: str
    description: str
    repetitions: int
    duration: int
    getSet: int
    intensity: int = 0


class CalcExercise(Exercice):
    _id: str
    calculatedDuration: int
    calculatedRest: int
    calculatedRepetitions: int


PatchExercise = create_partial_model(ExerciceDTO)
