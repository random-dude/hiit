from typing import List
from beanie import WriteRules
from bson.objectid import ObjectId

from login.models import JWTuser
from users.models import User, UserExercises
from exercises.models import *


async def addExercise(user: JWTuser, exercise: Exercice) -> Exercice:
    """add an exercise dict to the database and add its _id to the user's exercisesID list"""
    user = await User.find_one(User.username == user.username, fetch_links=True)
    user.exercises.append(exercise)
    user = await user.save(link_rule=WriteRules.WRITE)
    return exercise


async def getAllExercises(user: JWTuser) -> List[CalcExercise]:
    """returns all exercises in the db as a list of dict"""
    user = await User.find_one(
        User.username == user.username, fetch_links=True
    ).project(UserExercises)
    return [addCalculatedDurationsFromIntensity(ex) for ex in user.exercises]


async def patchExercise(user: JWTuser, id: str, exercise: Exercice) -> Exercice | None:
    id = ObjectId(id)
    userExercises = await User.find_one(
        User.username == user.username, fetch_links=True
    ).project(UserExercises)
    try:
        toUpdate = next(ex for ex in userExercises.exercises if ex.id == id)
        toUpdate = toUpdate.dict()
        toUpdate.update(exercise.dict(exclude_unset=True))
        updated = await Exercice.save(Exercice(**toUpdate))
        return updated
    except StopIteration:
        return None


async def deleteExercise(user: JWTuser, id: str) -> bool:
    user = await User.find_one(User.username == user.username, fetch_links=True)
    id = ObjectId(id)
    toDelete = [exercise for exercise in user.exercises if exercise.id == id]
    if toDelete:
        await Exercice.find_one(Exercice.id == id).delete()
        return True
    else:
        return False


# threeshold percent of the original duration after which
# the number of repetitions will be altered instead of the duration
intensityMaxAlteration = 0.33

# percent of the duration to increment/decrement when adjusting the intensity
intensityAdjustement = 0.1


def addCalculatedDurationsFromIntensity(exercise: Exercice) -> CalcExercise:
    if exercise.intensity == 0 or exercise.duration == 0:
        calculatedDuration = exercise.duration
        calculatedRepetitions = exercise.repetitions
    else:
        repetitionsOffset, intensityPercent = divmod(
            abs(exercise.intensity) * intensityAdjustement, intensityMaxAlteration
        )
        if exercise.intensity < 0:
            repetitionsOffset *= -1
            intensityPercent *= -1
        calculatedDuration = round(exercise.duration * (1 + intensityPercent))
        calculatedRepetitions = exercise.repetitions + repetitionsOffset

    calculatedRest = round(calculatedDuration / 2 - exercise.getSet)
    # TODO: handle repetitions < 0
    return CalcExercise(
        calculatedDuration=calculatedDuration,
        calculatedRest=calculatedRest,
        calculatedRepetitions=calculatedRepetitions,
        **exercise.dict(exclude_unset=True)
    )
