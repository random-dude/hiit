from fastapi import APIRouter, HTTPException, Depends, Response
from fastapi_login import LoginManager

from login.views import login
from login.models import JWTuser
from exercises.models import *
from exercises.services import *

router = APIRouter()


@router.get("/exercises", response_model=List[CalcExercise])
async def get_all_exercises(user: JWTuser = Depends(login)):
    return await getAllExercises(user)


@router.put("/exercises", response_model=CalcExercise, status_code=201)
async def add_exercise(exercise: Exercice, user: JWTuser = Depends(login)):
    exercise = await addExercise(user, exercise)
    return addCalculatedDurationsFromIntensity(exercise)


@router.patch("/exercises/{id}", response_model=CalcExercise)
async def patch_exercise(
    id: str, exercise: PatchExercise, user: JWTuser = Depends(login)
):
    updatedExercise = await patchExercise(user, id, exercise)
    if not updatedExercise:
        raise HTTPException(
            status_code=404,
            detail=f"unable to patch the exercise with id {id}: not found for user {user.username}",
        )
    return addCalculatedDurationsFromIntensity(updatedExercise)


@router.delete("/exercises/{id}", response_model=List[CalcExercise])
async def delete_exercise(id: str, user: JWTuser = Depends(login)):
    deleted = await deleteExercise(user, id)
    if not deleted:
        raise HTTPException(
            status_code=404,
            detail=f"unable to delete the exercise with id {id}: not found for user {user.username}",
        )
    return await getAllExercises(user)
