from fastapi import FastAPI, HTTPException, Depends, Response, status, Request
from fastapi.middleware.cors import CORSMiddleware
from fastapi.security import OAuth2PasswordRequestForm

import motor.motor_asyncio
from motor.motor_asyncio import AsyncIOMotorClient
from beanie import init_beanie
from typing import List, Dict
from datetime import timedelta
import os

from db import config, Database

# from exercises import views as exerciseViews
# from login import views as loginViews
from exercises.views import router as exercisesRouter
from login.views import router as loginRouter
from users.views import router as userRouter
from login.views import login
from users.models import User
from exercises.models import Exercice

from setup_new_db import init_database
import defaults

app = FastAPI()
db = Database()

origins = config.API.origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(loginRouter)
app.include_router(exercisesRouter)
app.include_router(userRouter)


@app.on_event("startup")
async def startup_db_client():
    mongoDB_URI = f'mongodb://{os.environ["DB_USER"]}:{os.environ["DB_PASSWORD"]}@{os.environ["DB_HOST"]}'
    print("connecting to db:", mongoDB_URI)
    app.mongodb_client = AsyncIOMotorClient(mongoDB_URI)
    await init_beanie(
        database=app.mongodb_client[os.environ["DB_NAME"]],
        document_models=[Exercice, User],
    )
    app.database = app.mongodb_client[os.environ["DB_NAME"]]
    db.database = app.database
    db.users = app.database["users"]
    # init_database(app.database)


@app.on_event("shutdown")
def shutdown_db_client():
    app.mongodb_client.close()
