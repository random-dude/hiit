## signup and login

### signup requirements

* The sign in process should be a one step process as unobtrusive as possible.
* All required fields should be displayed at once with validation issues dynamically reported on the same view.
* Required fields includes:
  * username
* * password
  * password verification
  * challenge
* A simple random challenge should be completed to avoid bot registration. Emphasis is placed on the ease of use and accessibility over security.
* Validation:
  * the username should be provided (without constraints)
  * the username should be unique
  * a password should be provided (without constraints)
  * password verification should match the provided password
  * the challenge needs to be successfully completed
  * validation errors will be shown in place near the invalid field
  * validation will happen dynamically upon field focus loss
  * the validation button will be disabled as long as one of thoses criteria isn't fullfilled
* On completion, a short welcome message will inform the user and a CTA will allow it to switch to the empty exercises list view
* A link should transform the view to a login view for registered user

### login requirements

* the login button should be accessible everywhere on the onboarding (sticky bar ?)
* the login form will display seamlessly inside a drawer or a modal
* required fields:
  * login
  * password
* validation:
  * login is not empty
  * password is not empty
  * login and password are valid credentials, no individual validation will be provided (username exists but wrong password will be treated as if the username didn't exist)
* an error message will be displayed near the submit button if the credentials are invalid
* A link should transform the view to a signup view for unregistered user

### non-functional

* The signup view may open inside a drawer, modal or directly within the onboarding view, the less obtrusive the better (opening the signup view from the onboarding should not feel like navigating).
* Displaying all items at once may reduce the hindrance since we are one step away from using the service.
* User should not have to look for the login/signup button
