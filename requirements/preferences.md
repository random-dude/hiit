## user preferences

* User preferences may be access from the logged in homepage (dashboard ?) but not in play mode
* The following preferences can be edited:
  * enable audio alerts (ON/OFF switch): to allow notifications to play a sound when a timer starts
  * work audio (audio file): the audio file to be played when a work timer starts
  * rest audio (audio file): the audio file to be played when a resting timer starts
  * get ready audio (audio file): the audio file to be played when the user should ready himself to work
  * congrats audio (audio file): the audio file to be played when the exercise is done

