## landing page

* The homepage should brievely introduce the user to the project and invite him to sign up.
* It might be a simple SPA with a few hero style sections.
* RGPD: only one cookie is set when signing up or loging in for authentication purpose. No third parties cookies or trackers.

### Project guidelines

(actual layout TBD)

* Make the most out of your workout. Improve your efficiency by using precise timings.
  * Repeatability allow you to max out the exercise intensity each and every time.
  * Plan ahead your session, the total duration is displayed upfront.
* Create the exercise that fits you. Ajust anytime.
  * At the gym, at home or on the go, HIIT can be applied to nearly every workout activity.
  * Use audio cues and let the app run in the background while listening to music or podcasts.
  * Smart intensity adjustment: ease or increase the exercise intensity when needed. Reset anytime.
* Achieve your goals and track your improvements.
  * Record sessions and time spent working out, for your eyes only.
  * Track intensity adjustment and overall duration changes.
* Forever free and open-source, no ads, no data shared with third parties.
