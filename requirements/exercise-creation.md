## Exercise creation and edit

### Title and description

* The user must fill up a title
* An HTML/rich text description may be optionally provided. It may contain images.
* The user can choose to either enter a duration (in seconds) or to record it.

### Duration / record mode

To find the maximum exercise duration, the user can workout when timed up. The recorded duration is then cut back by a few percents to account for multiple repetitions.

* The user can set a *get set* countdown before the timer actually starts.
* The countdown duration is easily adjustable either by a slider or a few presets buttons.
* A start button is show and will start the timer/countdown when pressed. It should be rather big.
* If a coundown has been selected, it will be shown with a corresponding audio cue.
* When started, an "infinite loader" will be shown with a big and easily accessible stop button.
* When stopped, the duration is shown with a few presets to cut it back (5, 10, 15, 20%)
* A retry button will allow the user to start again from the beginning.

### Repetitions / get set

* After entering the duration, the user must input the repetition count (defaults to 10).
* A get set period can also be set optionally. If the record mode was used and a countdown was set, it will default to the same value.
* A save button will notify the user of the successful creation (or any error that might occur).

### Edit mode

When editing a previously created exercise, the same view will be shown, only with each value already filled in.
