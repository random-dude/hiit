## Play mode requirements

Play mode is shown when an exercise is selected, either manually or as part of a routine. It truly is the heart of this application.

### Goal

The goal of the play mode is to allow an user to workout for precise amounts of time, with timed rests between workout sessions. For practical reasons, audio cues will signal the user when to work, when to rest and when to prepare to workout again.

The duration of the workout sessions and the number of repetitions are defined by the user when creating the exercise.

* If the exercise is manually selected, tapping a large area/button will begin the exercise.
* The exercise can be in one of six states:
  * **stopped** (inactive): waiting for the user to begin
  * **get ready:** (active, optional) counting down while the user set itself up for the exercise
  * **work**: (active) counting down the work period
  * **rest:** (active) counting down the resting period
  * **paused:** (inactive) but ready to resume
  * **done:** (inactive) the exercise has been completed and the routine is waiting to continue or the user can select another exercise
* The exercise is started either manually by the user or automatically if part of a routine.

### Active states

* When started, the states will seamlessly cycle from **get-ready** (if available for this exercise) to **work**, then **rest** then cycle again for as many repetitions as the user has defined in the exercise.
* **get-ready** and **work** will appear for the duration set by the user when creating the exercise, **rest** for half of the **work** duration minus the **get-ready** period if defined.
* On each state change, a specific audio cue is played (one for each state).
* The exercise description isn't needed anymore when started.
* Availables actions in active states:
  * pause / resume
  * increase / decrease intensity
  * stop and go back to the routine selector
  * stop and load another exercise
* an animated circular gauge will always show the remaining time of the current state.
* an animated linear gauge should show the total progression of the exercise.
* During the exercise, it's intensity can be increased or decreased. This will impact the duration of the work and rest period. By clicking a **+** or **-** button the durations will be altered until reaching a given threeshold (altering the durations for more than 50% total ?) after which the same action from the user may alter the repetitions count instead.

### Inactive states

* When **stopped**:
  * The exercise title and description will be shown. The description may be short or long and contain images.
  * Exercise intensity may be adjusted.
* When **done**:
  * A congratulation message/animation will be shown.
  * If the exercise is part of a routine, a gauge counting down the time remaining before the next exercise starts will be shown. The name and description of the next exercise will replace the current one shortly after the congratulation message has faded.
* when pausing / completing or stopping the exercise, the total time spent on the exercise will be updated in DB in order to display stats later on.

### non-functional

* when the exercise starts, the user should be able to guesstimate at a glance:
  * what state is the exercise in (color coded ?)
  * how much time is remaining for the current state, an animated gauge is preferred over actual values
  * how much of the whole exercise has been done, again an animated gauge is preferred over digits.
* Since the user may be in an uncomfortable position with poor viewing conditions, the view should be as uncluttered and high constrast as possible and the gauges should be larges and easy to read.
* For the same reasons, start/pause/resume action buttons or at least their interactive areas should be oversized enough to be clicked while on a threadmill for example.
